<h1 align=center>🌊 unixsea's config files.</h1>

 ## Information 🦐
*WM* - Xmonad [(config)](https://github.com/unixsea/xmonad)

*Bar* - Polybar

*IDE* - Neovim

*Music* - MPD / NCMPCPP

*Terminal* - Kitty

*Shell* - Fish (OMF)

## Mentions 🍥
[(Angelo Fallaria)](https://github.com/angelofallars) FOR THE NEOFETCH, NEOVIM, TERMINAL

[(Luke Smith)](https://github.com/LukeSmithxyz) FOR THE MPD AND NCMPCPP


## Images
![e](https://i.imgur.com/28co6iM.png)
![v](https://i.imgur.com/Wg56oRS.png)
![d](https://i.imgur.com/nGT0n1F.png)
![b](https://i.imgur.com/86OT3Dn.png)
